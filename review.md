---
documentclass: scrartcl
classoption:
  - DIV10
lang: en-US
papersize: a4
header-includes:
  - \usepackage{isodate}
fontfamily: sourcesanspro
author-meta:
  - Reviewer



date: \today
subject: Conference of Journal
title: The paper Title
author:
  - John Does
  - Jane Does
received: 2021-01-01
---

## Blind Comments to The Authors

//Neutral summary//

//Discussion//

- What is the contribution of the paper?
- Does the author explain the significance of this paper?
- Is the paper clearly written and well organized?
- Does the introduction state the purpose of the paper?
- Are the references relevant and complete? Supply missing references.
- If the paper is not technically sound, what are the deficiencies


## Overall Recommendation



## Major Issues

1. List all
1. Major Issues
1. Here


## Minor Issues

1. List all
1. Minor issues
1. Here


## Confidential Comment to the Editor

