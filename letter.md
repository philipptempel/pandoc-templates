---
papersize: a4
header-includes:
  - \usepackage{isodate}
  - \renewcommand{\familydefault}{\sfdefault}
  - \usepackage{lmodern}
  - \usepackage{marvosym}
lang: de-DE
date: \today

recipient: |
  | Jane Doe
  | 123 Main Street
  | 23456 Maintownington
  | USA

fromname: Jon Doe
fromaddress: |
  | 1 First Avenue
  | 98765 First State
fromphone: +1 234 567 8910
fromemail: mail@example.com
fromalign: right

location: |
  | Mitglied Nr. 4711
  | seit dem 11.09.2001
  | Vorsitzender in den Jahren 2003--2005

# title: Title of letter
subject: Subject
# subjectoption: underlined

place: Musterheim

opening: Liebe Vereinsvorsitzende,
closing: Yours sincerely,
ps: "PS: Ich hoffe, Du nimmst mir das nicht krumm."
encl: Auszug aus der Satzung, in dem die Mitgliederversammlungen geregelt sind
cc: |
  | Die Vereinsvorsitzende
  | Alle Mitglieder

---

Hello
