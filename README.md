# Pandoc Templates

This is a collection of my own pandoc templates for creating various documents from markdown.
The templates are all contained within the `templates` directory, which should be symlinked to `~/.pandoc/templates` (create the `~/.pandoc` directory if it does not already exist).